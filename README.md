![alt text](BSF-logo.png)


# Mindset & Motivation

## Motivation, Disipline and Dedication

TODO

## Body dismorphia and preventing eating disorders

TODO

<!------------------------------------------------------------------>

# Sleep

TODO

<!------------------------------------------------------------------>

# Cardio

# What is good cardio?

TODO

# How much cardio should i do?

# Resistance Training 

## What routine should I do as a beginner?

1. First before you should learn the form of all compound exercises such as bench press, deadlfit, barbell row, squat, shoulder press, and how to use machines. 

2. Once you have an idea of the form of each major exercise you should look towards doing a full body or upper lower split, which allows you to train the body at-least twice a week. 

https://docs.google.com/spreadsheets/d/1H_JtRKExp5IEGR75JJJkp1PV8TixkckRQp4BeJfN5kc/edit#gid=1316631277 <-- if you want to build strength GZCLP 3 day template
RP-MPT-Novice file is a more hypertrophy focussed full body program 
If you need more help ask @Personal trainer

## At home training program

It is possible to build a great physique without spending too much money on a gym membership or expensive equipment.
But if you are planning to train from home you should consider buying the following equipment to make training easier and more fun.

* Pull up bar
* A dumbell set, preferably over 2x12kg

If you are planning to spend a little more money on a home gym the following equipment is recommended:

* Barbell
* Adjustable Bench
* Squat rack

## What Training split should I do?

* Push Pull Legs
* Push Pull
* Full body
* Upper lower

## How do I get abbs?

TODO

## hypertrophy explained

TODO

* [Hypertrophy made simple](https://www.youtube.com/watch?v=4cW0EmO12Lk&list=PLyqKj7LwU2RukxJbBHi9BtEuYYKm9UqQQ&index=1)

## Muscle measurements and comparison tools

* https://bodywhat.com/
* https://ffmicalculator.org/

<!------------------------------------------------------------------>

# Nutrition

## How do I lose fat? 
To lose fat go into a calorie deficit, your body will seek energy from glycogen stores, and fat stores in your body and start burning them to get the energy they need, thus you will lose fat in the process. Cardio can help improve fat burning efficiency and mobilisation of fat, as well as attributing to a calorie deficit since cardio burns energy.

## How do you know if you are in a caloric deficit?

TODO, but this calculator might help:
https://www.calculator.net/calorie-calculator.html

## Hydration

TODO

## Salt

## Recepies

* [Ethan Chlebowski lower calorie series](https://www.youtube.com/results?search_query=+Ethan+Chlebowski+lower+calorie+series)
* [coach greg's anabolic recepies](https://www.youtube.com/results?search_query=coach+greg%27s+anabolic+recepies)
<!------------------------------------------------------------------>

# Fitness myths

## Fat spot reduction

You can't lose fat in one spot. You have to lose overall body fat.

<!------------------------------------------------------------------>

# Recommended external blogs/channels

Bodybuilding/fitness:
* [Jeff Nippard](https://www.youtube.com/user/icecream4PRs)
* [VitruvianPhysique](https://www.youtube.com/user/VitruvianPhysique)
* [Scooby werkstat](https://www.youtube.com/user/VitruvianPhysique)

strength training:
* [Alen Thrall](https://www.youtube.com/channel/UCRLOLGZl3-QTaJfLmAKgoAw)

Cooking and nutrition:
* [Ethan Chlebowski](https://www.youtube.com/channel/UCDq5v10l4wkV5-ZBIJJFbzQ)